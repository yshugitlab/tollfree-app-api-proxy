# Toll free Rest API proxy
nginix proxy app for our toll-free app API
## Usage


### Environment variables
* `LISTEN_PORT`- Port to listen on (default:`8000`)
* `APP_HOST` - hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default:`9000`)
